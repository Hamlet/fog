--[[
	Fate of the Gods - Adds roleplay mechanics.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedures
--

-- Argument: username_tool_xp
fog.pr_ToolXPwrite = function(a_s_keyword)
	local i_experience = fog.fn_ToolXPread(a_s_keyword)

	i_experience = (i_experience + 1)

	fog.t_MOD_STORAGE:set_int(a_s_keyword, i_experience)
end


-- Arguments:
-- username_tool_lvl
-- i_lvl
fog.pr_ToolLVLwrite = function(a_s_keyword, a_i_lvl)
	fog.t_MOD_STORAGE:set_int(a_s_keyword, a_i_lvl)
end


-- Arguments: a_t_itemstack
-- a_i_level
-- a_s_type
fog.pr_ToolOverrider = function(a_t_itemstack, a_i_level, a_s_type)
	local t_TOOL_CAPABILITIES = {}

	-- Diamond capabilities
	if (a_i_level == 1) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.9,
				max_drop_level=1,
				groupcaps={
					choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, 
							uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=7},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.9,
				max_drop_level=3,
				groupcaps={
					cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, 
								uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=5},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=1,
				groupcaps={
					crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, 	
								uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.7,
				max_drop_level=1,
				groupcaps={
					snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, 
							uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=8},
			}

		end

	-- Mese capabilities
	elseif (a_i_level == 2) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.9,
				max_drop_level=1,
				groupcaps={
					choppy={times={[1]=2.20, [2]=1.00, [3]=0.60}, 
							uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=6},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.9,
				max_drop_level=3,
				groupcaps={
					cracky = {times={[1]=2.4, [2]=1.2, [3]=0.60}, 
								uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=5},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=3,
				groupcaps={
					crumbly = {times={[1]=1.20, [2]=0.60, [3]=0.30}, 
								uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.7,
				max_drop_level=1,
				groupcaps={
					snappy={times={[1]=2.0, [2]=1.00, [3]=0.35}, 
							uses=i_TOOL_USES, maxlevel=3},
				},
				damage_groups = {fleshy=7},
			}

		end

	-- Steel capabilities
	elseif (a_i_level == 3) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=1,
				groupcaps={
					choppy={times={[1]=2.50, [2]=1.40, [3]=1.00}, 
							uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=1,
				groupcaps={
					cracky = {times={[1]=4.00, [2]=1.60, [3]=0.80}, 
								uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.1,
				max_drop_level=1,
				groupcaps={
					crumbly = {times={[1]=1.50, [2]=0.90, [3]=0.40}, 
								uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=3},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.8,
				max_drop_level=1,
				groupcaps={
					snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, 
							uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=6},
			}

		end

	-- Bronze capabilities
	elseif (a_i_level == 4) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=1,
				groupcaps={
					choppy={times={[1]=2.75, [2]=1.70, [3]=1.15}, 
							uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=1,
				groupcaps={
					cracky = {times={[1]=4.50, [2]=1.80, [3]=0.90}, 
								uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=4},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.1,
				max_drop_level=1,
				groupcaps={
					crumbly = {times={[1]=1.65, [2]=1.05, [3]=0.45}, 
								uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=3},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 0.8,
				max_drop_level=1,
				groupcaps={
					snappy={times={[1]=2.75, [2]=1.30, [3]=0.375}, 
							uses=i_TOOL_USES, maxlevel=2},
				},
				damage_groups = {fleshy=6},
			}

		end

	-- Stone capabilities
	elseif (a_i_level == 5) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.2,
				max_drop_level=0,
				groupcaps={
					choppy={times={[1]=3.00, [2]=2.00, [3]=1.30}, 
							uses=i_TOOL_USES, maxlevel=1},
				},
				damage_groups = {fleshy=3},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.3,
				max_drop_level=0,
				groupcaps={
					cracky = {times={[2]=2.0, [3]=1.00}, 
								uses=i_TOOL_USES, maxlevel=1},
				},
				damage_groups = {fleshy=3},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.4,
				max_drop_level=0,
				groupcaps={
					crumbly = {times={[1]=1.80, [2]=1.20, [3]=0.50}, 
								uses=i_TOOL_USES, maxlevel=1},
				},
				damage_groups = {fleshy=2},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.2,
				max_drop_level=0,
				groupcaps={
					snappy={times={[2]=1.4, [3]=0.40}, uses=i_TOOL_USES, 
							maxlevel=1},
				},
				damage_groups = {fleshy=4},
			}

		end

	-- Wood capabilities
	elseif (a_i_level == 6) then
		if (a_s_type == 'axe') then
			local i_TOOL_USES = fog.fn_ToolsAxesUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.0,
				max_drop_level=0,
				groupcaps={
					choppy = {times={[2]=3.00, [3]=1.60}, uses=i_TOOL_USES, 
								maxlevel=1},
				},
				damage_groups = {fleshy=2},
			}

		elseif (a_s_type == 'pick') then
			local i_TOOL_USES = fog.fn_ToolsPicksUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.2,
				max_drop_level=0,
				groupcaps={
					cracky = {times={[3]=1.60}, uses=i_TOOL_USES, maxlevel=1},
				},
				damage_groups = {fleshy=2},
			}

		elseif (a_s_type == 'shovel') then
			local i_TOOL_USES = fog.fn_ToolsShovelsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1.2,
				max_drop_level=0,
				groupcaps={
					crumbly = {times={[1]=3.00, [2]=1.60, [3]=0.60}, 
								uses=i_TOOL_USES, maxlevel=1},
				},
				damage_groups = {fleshy=2},
			}

		elseif (a_s_type == 'sword') then
			local i_TOOL_USES = fog.fn_ToolsSwordsUses(a_t_itemstack)

			t_TOOL_CAPABILITIES = {
				full_punch_interval = 1,
				max_drop_level=0,
				groupcaps={
					snappy={times={[2]=1.6, [3]=0.40}, uses=i_TOOL_USES, 
							maxlevel=1},
				},
				damage_groups = {fleshy=2},
			}

		end

	end

	a_t_itemstack:get_meta():set_tool_capabilities(t_TOOL_CAPABILITIES)
end
