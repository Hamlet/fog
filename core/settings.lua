--[[
	Fate of the Gods - Adds roleplay mechanics.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

fog.i_CLASS_F = 999		-- 0 to Class F		= Wood capabilities
fog.i_CLASS_E = 9999	-- 1000 to Class E	= Stone capabilities
fog.i_CLASS_D = 14999	-- 10000 to Class D	= Bronze capabilities
fog.i_CLASS_C = 19999	-- 15000 to Class C	= Steel capabilities
fog.i_CLASS_B = 29999	-- 20000 to Class B	= Mese capabilities
						-- >= 30000	 		= Diamond capabilities
