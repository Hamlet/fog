--[[
	Fate of the Gods - Adds roleplay mechanics.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedure
--

local pr_OverrideSwords = function()
	local t_list = {
		'default:sword_wood', 'default:sword_stone', 'default:sword_bronze',
		'default:sword_steel', 'default:sword_mese', 'default:sword_diamond'
	}

	for i_element = 1, 6 do
		minetest.override_item(t_list[i_element], {
			after_use = function(itemstack, user, node, digparams)

				itemstack:add_wear(digparams.wear)

				if (user:is_player() == true) then
					local s_USER_NAME = user:get_player_name()
					local s_KEY_XP = s_USER_NAME .. '_i_xp_sword'
					local s_KEY_RANK = s_USER_NAME .. '_i_rank_sword'
					local i_CURRENT_XP = 0
					local i_CURRENT_CLASS = 0

					-- Add the new experience points
					fog.pr_ToolXPwrite(s_KEY_XP)

					-- Update the associated variable
					i_CURRENT_XP = fog.fn_ToolXPread(s_KEY_XP)

					-- Update the level rank
					i_CURRENT_CLASS = fog.fn_ToolClassCheck(i_CURRENT_XP)

					-- Save the rank for the current tool
					fog.pr_ToolLVLwrite(s_KEY_RANK, i_CURRENT_CLASS)

					-- Update the wield item tool capabilities
					fog.pr_ToolOverrider(itemstack, i_CURRENT_CLASS, 'sword')

					-- Update the character's sheet
					fog.pr_OverrideFormspec()
				end

				return itemstack
			end
		})
	end
end


--
-- Main body
--

pr_OverrideSwords()
