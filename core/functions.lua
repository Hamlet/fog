--[[
	Fate of the Gods - Adds roleplay mechanics.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Functions
--

-- Argument: username_tool_xp
fog.fn_ToolXPread = function(a_s_keyword)
	local i_experience = fog.t_MOD_STORAGE:get_int(a_s_keyword)

	if (i_experience == nil) then
		i_experience = 0
	end

	return i_experience
end


-- Argument: username_tool_lvl
fog.fn_ToolLVLread = function(a_s_keyword)
	local i_level = fog.t_MOD_STORAGE:get_int(a_s_keyword)

	if (i_level == nil) then
		i_level = 0
	end

	return i_level
end


-- Argument: username_tool_xp
fog.fn_ToolClassCheck = function(a_i_tool_xp)
	local i_class = 0

	-- Diamond capabilities
	if (a_i_tool_xp > fog.i_CLASS_B) then
		i_class = 1

	-- Mese capabilities
	elseif (a_i_tool_xp <= fog.i_CLASS_B)
		and (a_i_tool_xp > fog.i_CLASS_C) then
		i_class = 2

	-- Steel capabilities
	elseif (a_i_tool_xp <= fog.i_CLASS_C)
		and (a_i_tool_xp > fog.i_CLASS_D) then
		i_class = 3

	-- Bronze capabilities
	elseif (a_i_tool_xp <= fog.i_CLASS_D)
		and (a_i_tool_xp > fog.i_CLASS_E) then
		i_class = 4

	-- Stone capabilities
	elseif (a_i_tool_xp <= fog.i_CLASS_E)
		and (a_i_tool_xp > fog.i_CLASS_F) then
		i_class = 5

	-- Wood capabilities
	else
		i_class = 6

	end

	print('Tool class: ' .. i_class)

	return i_class
end

-- Used to determine an item's durability
fog.fn_ToolsAxesUses = function(a_t_itemstack)
	local s_ITEM_NAME = a_t_itemstack:get_name()
	local i_ITEM_USES = 0

	--print("Item name: " .. s_ITEM_NAME)

	if (string.match(s_ITEM_NAME, 'wood') ~= nil) then
		i_ITEM_USES = 10

	elseif (string.match(s_ITEM_NAME, 'stone') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'bronze') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'steel') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'mese') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'diamond') ~= nil) then
		i_ITEM_USES = 30

	end

	return i_ITEM_USES
end

-- Used to determine an item's durability
fog.fn_ToolsPicksUses = function(a_t_itemstack)
	local s_ITEM_NAME = a_t_itemstack:get_name()
	local i_ITEM_USES = 0

	--print("Item name: " .. s_ITEM_NAME)

	if (string.match(s_ITEM_NAME, 'wood') ~= nil) then
		i_ITEM_USES = 10

	elseif (string.match(s_ITEM_NAME, 'stone') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'bronze') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'steel') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'mese') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'diamond') ~= nil) then
		i_ITEM_USES = 30

	end

	return i_ITEM_USES
end

-- Used to determine an item's durability
fog.fn_ToolsShovelsUses = function(a_t_itemstack)
	local s_ITEM_NAME = a_t_itemstack:get_name()
	local i_ITEM_USES = 0

	--print("Item name: " .. s_ITEM_NAME)

	if (string.match(s_ITEM_NAME, 'wood') ~= nil) then
		i_ITEM_USES = 10

	elseif (string.match(s_ITEM_NAME, 'stone') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'bronze') ~= nil) then
		i_ITEM_USES = 25

	elseif (string.match(s_ITEM_NAME, 'steel') ~= nil) then
		i_ITEM_USES = 30

	elseif (string.match(s_ITEM_NAME, 'mese') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'diamond') ~= nil) then
		i_ITEM_USES = 30

	end

	return i_ITEM_USES
end

-- Used to determine an item's durability
fog.fn_ToolsSwordsUses = function(a_t_itemstack)
	local s_ITEM_NAME = a_t_itemstack:get_name()
	local i_ITEM_USES = 0

	--print("Item name: " .. s_ITEM_NAME)

	if (string.match(s_ITEM_NAME, 'wood') ~= nil) then
		i_ITEM_USES = 10

	elseif (string.match(s_ITEM_NAME, 'stone') ~= nil) then
		i_ITEM_USES = 20

	elseif (string.match(s_ITEM_NAME, 'bronze') ~= nil) then
		i_ITEM_USES = 25

	elseif (string.match(s_ITEM_NAME, 'steel') ~= nil) then
		i_ITEM_USES = 30

	elseif (string.match(s_ITEM_NAME, 'mese') ~= nil) then
		i_ITEM_USES = 30

	elseif (string.match(s_ITEM_NAME, 'diamond') ~= nil) then
		i_ITEM_USES = 40

	end

	return i_ITEM_USES
end
