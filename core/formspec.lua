--[[
	Fate of the Gods - Adds roleplay mechanics.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- This is used to register the charaxter's sheet inventory tab
sfinv.register_page('fog:charsheet', {
	title = fog.l10n('P.C. sheet'),
	get = function(self, player, context)

		local s_formspecString = fog.fn_FogMTGstats(player)

		return sfinv.make_formspec(player, context,
				'textlist[0,0;7.8,4.5;;' .. s_formspecString .. ']', true)
	end
})


-- This is used to update the character's sheet inventory tab
fog.fn_FogMTGstats = function(a_s_player)

	-- Constants
	local s_LEVEL_AXE = fog.l10n('   Axe class: ');
	local s_LEVEL_PICK = fog.l10n('   Pick class: ');
	local s_LEVEL_SHOVEL = fog.l10n('   Shovel class: ');
	local s_LEVEL_SWORD = fog.l10n('   Sword class: ');

	-- Variables
	local s_formspecText = '';
	local s_keyword = a_s_player:get_player_name() .. '_';

	local i_levelAxe, i_levelPick, i_levelShovel, i_levelSword,
		i_expAxe, i_expPick, i_expShovel, i_expSword

	i_levelAxe = fog.fn_ToolLVLread(s_keyword .. 'i_rank_axe');
	i_levelPick = fog.fn_ToolLVLread(s_keyword .. 'i_rank_pick');
	i_levelShovel = fog.fn_ToolLVLread(s_keyword .. 'i_rank_shovel');
	i_levelSword = fog.fn_ToolLVLread(s_keyword .. 'i_rank_sword');

	i_expAxe = fog.fn_ToolXPread(s_keyword .. 'i_xp_axe')
	i_expPick = fog.fn_ToolXPread(s_keyword .. 'i_xp_pick')
	i_expShovel = fog.fn_ToolXPread(s_keyword .. 'i_xp_shovel')
	i_expSword = fog.fn_ToolXPread(s_keyword .. 'i_xp_sword')

	s_formspecText = ',' ..
		s_LEVEL_AXE .. i_levelAxe ..
		'   (' .. i_expAxe .. fog.l10n(' experience points)') .. ',' ..

		s_LEVEL_PICK .. i_levelPick ..
		'   (' .. i_expPick .. fog.l10n(' experience points)') .. ',' ..

		s_LEVEL_SHOVEL .. i_levelShovel ..
		'   (' .. i_expShovel .. fog.l10n(' experience points)') .. ',' ..

		s_LEVEL_SWORD .. i_levelSword ..
		'   (' .. i_expSword .. fog.l10n(' experience points)')

	return s_formspecText
end


-- This is used to override the character's sheet page
fog.pr_OverrideFormspec = function()
	sfinv.override_page('fog:charsheet', {
		title = fog.l10n('P.C. sheet'),
		get = function(self, player, context)

			local s_formspecString = fog.fn_FogMTGstats(player)

			return sfinv.make_formspec(player, context,
					'textlist[0,0;7.8,4.5;;' .. s_formspecString .. ']', true)
		end
	})
end
